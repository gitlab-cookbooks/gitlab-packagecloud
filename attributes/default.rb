default['gitlab-packagecloud']['data_filesystem_uuid'] = nil
default['gitlab-packagecloud']['package_cloud_master_token'] = nil
default['gitlab-packagecloud']['snitch_url'] = nil
default['gitlab-packagecloud']['users'] = []

default['gitlab-packagecloud']['packagecloud_rb']['mysql_rds_migration'] = false

default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_database_host'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_database_user'] = ''
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_database_password'] = ''
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_database_port'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_database_name'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_database_ssl'] = false
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_rds_ssl'] = false

default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_redis_host'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_redis_user'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_redis_password'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_redis_port'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_redis_database_number'] = nil

default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_enterprise_license_key'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_api_token'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_aws_access_key'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_aws_secret_key'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_aws_host'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_cloudfront_enabled'] = false
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_cloudfront_secret'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_distribution_id'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_cloudfront_packages_domain'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_backup_keep_num'] = 14
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_backup_xbstream'] = true
default['gitlab-packagecloud']['packagecloud_rb']['packagecloud_rails_backup_threads'] = 6
default['gitlab-packagecloud']['packagecloud_rb']['smtp_user_name'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['smtp_password'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['unicorn_worker_timeout'] = 300
default['gitlab-packagecloud']['packagecloud_rb']['unicorn_worker_processes'] = 8
default['gitlab-packagecloud']['packagecloud_rb']['rainbows_worker_processes'] = 8
default['gitlab-packagecloud']['packagelcoud_rb']['unicorn_backlog_socket'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['nginx_custom_packagecloud_server_config'] = nil
default['gitlab-packagecloud']['packagecloud_rb']['nginx_client_max_body_size'] = 500
default['gitlab-packagecloud']['packagecloud_rb']['resque_delete_worker_count'] = 1
default['gitlab-packagecloud']['packagecloud_rb']['resque_index_worker_count'] = 6
default['gitlab-packagecloud']['packagecloud_rb']['nginx_ssl_protocols'] = 'TLSv1.2 TLSv1.3'
default['gitlab-packagecloud']['packagecloud_rb']['nginx_ssl_ciphers'] =
  'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:' \
  'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:' \
  'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:' \
  'DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384'

default['gitlab-packagecloud']['notification']['email'] = 'root'
