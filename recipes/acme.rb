include_recipe 'acme'

node.override['acme']['contact'] = ['ops-contact+packagecloud-le@gitlab.com']
node.override['acme']['endpoint'] = 'https://acme-v01.api.letsencrypt.org'

acme_certificate 'packages.gitlab.com' do
  crt     '/etc/packagecloud/ssl/server.crt'
  key     '/etc/packagecloud/ssl/server.key'
  wwwroot '/opt/packagecloud/embedded/service/packagecloud-rails/public'
  notifies :run, 'bash[restart nginx]'
end
