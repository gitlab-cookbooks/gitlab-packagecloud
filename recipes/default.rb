# frozen_string_literal: true

# Cookbook:: gitlab-packagecloud
# Recipe:: default
# License:: MIT
#
# Copyright:: 2016 GitLab Inc.

include_recipe 'gitlab-vault'
gitlab_packagecloud_conf = GitLab::Vault.get(node, 'gitlab-packagecloud')

group 'packagecloud' do
  gid 888
  not_if { node['etc']['passwd']['packagecloud'] }
end

user 'packagecloud' do
  uid 888
  gid 'packagecloud'
  not_if { node['etc']['passwd']['packagecloud'] }
end

packagecloud_opt_dir = '/var/opt/packagecloud'

directory packagecloud_opt_dir do
  recursive true
end

mount packagecloud_opt_dir do
  device gitlab_packagecloud_conf['data_filesystem_uuid']
  device_type :uuid
  fstype 'ext4'
  options 'defaults,noatime,nofail'
  action :enable
end

packagecloud_repo 'computology/packagecloud-enterprise' do
  base_url 'https://packagecloud.io'
  type 'deb'
  master_token gitlab_packagecloud_conf['master_token']
end

# pkg_name = 'packagecloud'
# Remove hold from package to allow upgrade
# execute "apt-mark unhold #{pkg_name}"

# package pkg_name do
#   action :upgrade
#   notifies :run, 'execute[packagecloud-ctl reconfigure]'
#   notifies :run, 'bash[email]'
# end

# Prevent 'apt-get upgrade' from upgrading packagecloud
# execute "apt-mark hold #{pkg_name}"

directory '/etc/packagecloud'

template '/etc/packagecloud/packagecloud.rb' do
  owner 'root'
  group 'root'
  mode '0640' # match what packagecloud-ctl reconfigur does
  variables gitlab_packagecloud_conf['packagecloud_rb'].to_hash
  notifies :run, 'execute[packagecloud-ctl reconfigure]'
end

template '/etc/packagecloud/users.yml' do
  variables users: gitlab_packagecloud_conf['users']
  notifies :run, 'execute[packagecloud-ctl reconfigure]'
end

cookbook_file "#{Chef::Config[:file_cache_path]}/packagecloud-no-empty-filelists.patch" do
  source 'packagecloud-no-empty-filelists.patch'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

bash 'patch-packagecloud' do
  user 'root'
  code <<-EOH
    patch --silent \
          --directory /opt/packagecloud \
          --strip 1 \
          --reject-file - \
          < #{Chef::Config[:file_cache_path]}/packagecloud-no-empty-filelists.patch
  EOH
  only_if "patch --silent \
                 --directory /opt/packagecloud \
                 --dry-run \
                 --strip 1 \
                 --reject-file - \
                 --forward \
                 < #{Chef::Config[:file_cache_path]}/packagecloud-no-empty-filelists.patch"

  notifies :run, 'execute[packagecloud-ctl restart resque]'
  notifies :run, 'execute[packagecloud-ctl hup unicorn]'
end

execute 'packagecloud-ctl restart resque' do
  action :nothing
end

execute 'packagecloud-ctl hup unicorn' do
  action :nothing
end

execute 'packagecloud-ctl reconfigure' do
  action :nothing
  notifies :run, 'execute[packagecloud-ctl restart]'
end

execute 'packagecloud-ctl restart' do
  action :nothing
end

bash 'email' do
  code <<-EOS
    echo "Packagecloud has been upgraded!" | mail -s "Packagecloud has been upgraded!" #{gitlab_packagecloud_conf['notification']['email']}
  EOS
  action :nothing
end

ssl_dir = '/etc/packagecloud/ssl'
directory ssl_dir do
  mode '0700'
end

bash 'restart nginx' do
  code <<-EOS
  if packagecloud-ctl status nginx ; then
    packagecloud-ctl hup nginx
  fi
  EOS
  action :nothing
end

gpg_dir = '/etc/packagecloud/gpgkey'

directory gpg_dir do
  mode '0700'
  owner 'packagecloud'
  group 'packagecloud'
end

packagecloud_gpg = "/usr/bin/gpg --homedir #{gpg_dir}"

privkey = gitlab_packagecloud_conf['gpg_private_key']

key_id = IO.popen(%w(/usr/bin/gpg --homedir /dev/null --list-packets), 'r+') do |gpg|
  gpg.puts(privkey)
  gpg.close_write
  match = gpg.read.match(/\skeyid:[\s]+([\h]+)/)
  match[1] unless match.nil?
end

Chef::Log.info "Supplied packages@gitlab.com GPG private key ID: #{key_id}"

bash 'import GPG private key' do
  code <<-EOH
  find #{gpg_dir} -not -path #{gpg_dir} -delete # Avoid having two keys for packages@gitlab.com
  #{packagecloud_gpg} --import --allow-secret-key-import <<EOF
#{privkey}
EOF
  EOH
  user 'packagecloud'
  group 'packagecloud'
  not_if do
    local_key_id = `sudo -u packagecloud #{packagecloud_gpg} --with-colons --list-secret-keys packages@gitlab.com | awk '/^sec/ { split($1, a, ":"); print a[5] }'`.strip
    Chef::Log.info "Local GPG key ID for packages@gitlab.com: #{local_key_id}"
    Chef::Log.info "Comparing it to the expected GPG key ID for packages@gitlab.com: #{key_id}"
    local_key_id == key_id
  end
  notifies :run, 'execute[packagecloud-ctl reconfigure]'
end

cron 'sync backups with S3' do
  minute '0'
  hour '4'
  command "s3cmd sync --multipart-chunk-size-mb=512 --exclude '*' --include '*.tgz' --include '*.xbstream' /var/opt/packagecloud/backups/ s3://gitlab-packagecloud-db-backups/ &>> /tmp/s3cmd_sync.log"
  action :delete
end

include_recipe '::acme'

include_recipe '::license-cron'
